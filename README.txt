This repository is about moving a site from standard drupal directory setup to
a makefile (also multisiting) based one.

Instruments used for this operations are a working drush installation with the
registry_rebuild plugin installed.

In order to get the hang of it here are the steps:

1. Get this repo on your local machine using git:
git clone git@bitbucket.org:oxrc/get-your-makefile.git

2. Checkout the 'before' branch
git checkout before

3. Do a drupal site installation by visiting the /install.php on your local
setup
NOTE: The before branch has drupal 7.28 files and some contrib modules under
sites/all/modules/contrib that should be enabled in order for you to be able to
test the migration to a makefile based deployment.

4. After the site is up and running, go into the main directory and remove the
.git folder, so now you have an example site that you should migrate.

5. Remove all the directories from within 'sites/all', so now you have a
'broken' site with missing contrib modules
NOTE: You can check that by accessing the "admin/modules" drupal path

5. Go into the "sites" directory and check out the project again from git, but
this time into a directory that's named exactly like your site's domain
NOTE: On my setup I used http://mdemo/ to access the site, thus I run the
following command:
git clone git@bitbucket.org:oxrc/get-your-makefile.git mdemo

6. Switch into the new directory and checkout the 'after' branch
git checkout after

7. Run ./rebuild.sh or ./rebuild.bat in order to get your modules back
NOTE: Windows users make sure you run this from within a console that can run
both 'drush' and 'git' commands.

8. Run "drush rr" for the site to apply the new paths for the 'broken' modules.

9. You should now be able to access you site by visiting the same path. If all
works - congratulation, you're one step closer to automating builds :) If it
doesn't - feel free to post your question/problem on the issue tracker:
https://bitbucket.org/oxrc/get-your-makefile/issues
